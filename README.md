# Blackstones Legacy

A project for a [Warhammer 40k](https://en.wikipedia.org/wiki/Warhammer_40,000) campaign rule set combining the rules for "Kill Team" with the setting of "Blackstone Fortress" inspired by [Mordheim](https://en.wikipedia.org/wiki/Mordheim) with a little bit "Shadow War: Armageddon".